﻿using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SerhiiMyskiv.RobotChallange
{
    public class MyskivSerhiiAlgorithm : IRobotAlgorithm
    {
        public string Author
        {
            get { return "Myskiv Serhii"; }
        }

        public string Description
        {
            get { return "Demo for students"; }
        }


        //лічильник рівнів
        public MyskivSerhiiAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }
        void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
            RobotCount = 0;
        }
        public int RoundCount { get; set; }
        public int RobotCount { get; set; }
        public int allMyRobots = 10;
        // public Position NearestStation;
        //public Position NearesrStations;// = new Position(0,0);


        //дії робота
        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var myPosition = robots[robotToMoveIndex].Position;

            int RobotCountNearStation = 0;
            int NearbyStationsCount = 0;

            RobotCountNearStation = FindNearbyRobotsCounts(myPosition, robotToMoveIndex, robots);
            NearbyStationsCount = FindNearbyStationsCount(myPosition, robotToMoveIndex, robots, map);


            if (robots[robotToMoveIndex].Energy > 300 && RoundCount < 40 && allMyRobots < 100 & RobotCountNearStation < 2 && NearbyStationsCount >= 2)
            {
                allMyRobots++;
                return new CreateNewRobotCommand();
            }

            var nearestStation = FindNearestFreeStation(robots[robotToMoveIndex], map, robots);


            if (nearestStation == null)
            {
                var pos = FindNearestStation(robots[robotToMoveIndex], map, robots);
                if (pos == null)
                {
                    return null;
                }
                else if (pos == myPosition)
                {
                    return new CollectEnergyCommand();
                }
                else if (FindDistance(pos, myPosition) < robots[robotToMoveIndex].Energy)
                {
                    return new MoveCommand() { NewPosition = pos };
                }
            }
            if (nearestStation == myPosition)
            {
                return new CollectEnergyCommand();
            }
            else
            {
                if (FindDistance(myPosition, nearestStation) < robots[robotToMoveIndex].Energy)
                    return new MoveCommand() { NewPosition = nearestStation };
                else
                {
                    if (robots[robotToMoveIndex].Energy <= 100 && robots[robotToMoveIndex].Energy > 50 && map.GetNearbyResources(myPosition, 2).Count < 2 && (RoundCount < 40))
                    {
                        var pos = LowEnergyMove(nearestStation, myPosition, robotToMoveIndex, robots);
                        if (FindDistance(myPosition, new Position(myPosition.X + pos.X, myPosition.Y + pos.Y)) < robots[robotToMoveIndex].Energy)
                            return new MoveCommand() { NewPosition = new Position(myPosition.X + pos.X, myPosition.Y + pos.Y) };
                    }
                    else if (robots[robotToMoveIndex].Energy <= 50)
                    {
                        var pos = FindNearestStation(robots[robotToMoveIndex], map, robots);
                        if (pos == null)
                        {
                            return null;
                        }
                        else if (pos == myPosition)
                        {
                            return new CollectEnergyCommand();
                        }
                        else if (FindDistance(pos, myPosition) < robots[robotToMoveIndex].Energy)
                        {
                            return new MoveCommand() { NewPosition = pos };
                        }
                    }
                    return new CollectEnergyCommand();
                }
            }
        }


        public int FindNearbyStationsCount(Position myPosition, int robotToMoveIndex, IList<Robot.Common.Robot> robots, Map map)
        {
            int NearbyStationsCount = 0;
            var NearbyStations = map.GetNearbyResources(myPosition, 15);
            foreach (var station in NearbyStations)
            {
                if (IsStationFree(station.Position, robots[robotToMoveIndex], robots))
                {
                    NearbyStationsCount++;
                }
            }
            return NearbyStationsCount;
        }

        public Position LowEnergyMove(Position nearestStation, Position myPosition, int robotToMoveIndex, IList<Robot.Common.Robot> robots)
        {
            var newX = 2;
            var newY = 2;
            if (nearestStation.X < myPosition.X)
            {
                newX = -2;
            }
            else if (nearestStation.X == myPosition.X) newX = 0;
            if (nearestStation.Y < myPosition.Y)
            {
                newY = -2;
            }
            else if (nearestStation.Y == myPosition.Y) newY = 0;
            return new Position(newX, newY);
        }

        public Position FindNearestStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            var stations = map.GetNearbyResources(movingRobot.Position, 5);
            foreach (var station in stations)
            {
                var X = station.Position.X;
                var Y = station.Position.Y;
                for (int i = 0; i < 3; i++)
                {
                    for (int x = X - i; x <= X + i; x++)
                    {
                        for (int y = Y - i; y <= Y + i; y++)
                        {
                            if (IsStationFree(new Position(x, y), movingRobot, robots))
                            {
                                return new Position(x, y);
                            }
                        }
                    }
                }
            }
            return null;
        }

        public int FindNearbyRobotsCounts(Position myPosition, int robotToMoveIndex, IList<Robot.Common.Robot> robots)
        {
            int RobotCountNearStation = 0;
            for (int x = myPosition.X - 2; x < myPosition.X + 3; x++)
            {
                for (int y = myPosition.Y - 2; y < myPosition.Y + 3; y++)
                {
                    if (!IsCellFree(new Position(x, y), robots[robotToMoveIndex], robots))
                    {
                        RobotCountNearStation++;
                    }
                }
            }
            return RobotCountNearStation;
        }

        public int FindDistance(Position firstPos, Position secondPos)
        {
            return (secondPos.X - firstPos.X) * (secondPos.X - firstPos.X) + (secondPos.Y - firstPos.Y) * (secondPos.Y - firstPos.Y);
        }

        public Position FindNearestFreeStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation nearest = null;
            int minDistance = int.MaxValue;
            var nearestStations = map.GetNearbyResources(movingRobot.Position, 50);
            foreach (var station in map.Stations)
            {
                if (IsStationFree(station.Position, movingRobot, robots))
                {
                    var myPosition = movingRobot.Position;

                    int d = FindDistance(myPosition, new Position(station.Position.X, station.Position.Y));//DistanceHelper.FindDistance(station.Position, movingRobot.Position);
                    if (d < minDistance)
                    {
                        minDistance = d;
                        nearest = station;
                    }
                }
            }
            return nearest == null ? null : nearest.Position;
        }

        public bool IsStationFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                    {
                        if (robot.OwnerName == "Myskiv Serhii")
                            return false;
                        else
                            return true;
                    }
                }
            }
            return true;
        }

        public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot != movingRobot)
                {
                    if (robot.Position == cell)
                    {
                        return false;
                    }
                }
            }
            return true;
        }


    }
}
