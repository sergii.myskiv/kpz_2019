using System;
using Xunit;

namespace SerhiiMyskiv.RobotChallenge.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(2, 3) } };
            var algorithm = new MyskivSerhiiAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }
        [TestMethod]
        public void TestCollectCommand()
        {
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>() { new Robot.Common.Robot() { Energy = 200, Position = new Position(1, 1) } };
            var algorithm = new MyskivSerhiiAlgorithm();
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestCreate() // ���� �� ��������� ������ ������
        {
            var algorithm = new MyskivSerhiiAlgorithm();
            var map = new Map();
            var stationPosition = new Position(10, 10);
            var stationPosition1 = new Position(12, 10);
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPosition,
                RecoveryRate = 2
            });
            map.Stations.Add(new EnergyStation()
            {
                Energy = 1000,
                Position = stationPosition1,
                RecoveryRate = 2
            });
            var robots = new List<Robot.Common.Robot>()
    {
        new Robot.Common.Robot(){Energy = 2000, Position = new Position(0,0)},
    };
            var command = algorithm.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestFreeStation() //���� �� ����� �������
        {
            var myAlg = new MyskivSerhiiAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(1, 2), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(2, 3), RecoveryRate = 2 });
            Owner my = new Owner();
            Owner alien = new Owner();
            var robots = new List<Robot.Common.Robot>()
            {
                new Robot.Common.Robot(){ Energy = 100, Position = new Position(1, 2), OwnerName = alien.Name},
                 new Robot.Common.Robot(){ Energy = 100, Position = new Position(3, 3), OwnerName = my.Name}
            };
            var command = myAlg.IsStationFree(map.Stations[1].Position, robots[1], robots);
            Assert.AreEqual(command, true);
        }

        [TestMethod]
        public void TestJumpToAlienRobot() //���� ������ �� ������ ������
        {
            var myAlg = new MyskivSerhiiAlgorithm();
            var map = new Map();
            var stationPosition = new Position(99, 10);
            map.Stations.Add(
            new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 }
            );
            Owner my = new Owner();
            Owner alien = new Owner();
            var robots = new List<Robot.Common.Robot>(){
        new Robot.Common.Robot(){ Energy = 100, Position = new Position(9 , 10), OwnerName = my.Name},
        new Robot.Common.Robot(){ Energy = 2000, Position = new Position(10, 10), OwnerName = alien.Name}
        };
            var command = myAlg.DoStep(robots, 0, map);
            Assert.AreEqual(((MoveCommand)command).NewPosition, new Position(10, 10));
        }

        [TestMethod]
        public void TestRobotsNearStation() // ������ ��� �������
        {
            var algo = new MyskivSerhiiAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation()
            {
                Energy = 150,
                Position = new Position(5, 5),
                RecoveryRate
            = 10
            });
            var robots = new List<Robot.Common.Robot>
        {
        new Robot.Common.Robot() {Energy = 100, Position = new Position(5, 5)},
        new Robot.Common.Robot() {Energy = 100, Position = new Position(6, 6)},
        new Robot.Common.Robot() {Energy = 100, Position = new Position(5, 6)},
        new Robot.Common.Robot() {Energy = 100, Position = new Position(6, 5)},
        new Robot.Common.Robot() {Energy = 100, Position = new Position(5, 4)},
        new Robot.Common.Robot() {Energy = 100, Position = new Position(4, 5)},
        new Robot.Common.Robot() {Energy = 100, Position = new Position(10, 10)}
        };
            var command = algo.IsStationFree(map.Stations[0].Position, robots[6], robots);
            Assert.AreEqual(command, false);
        }

        [TestMethod]
        public void TestJumpToStation() // ���� ������� �� �������
        {
            var myAlg = new MyskivSerhiiAlgorithm();
            var map = new Map();
            var stationPosition = new Position(10, 10);
            map.Stations.Add(
                new EnergyStation() { Energy = 1000, Position = stationPosition, RecoveryRate = 2 }
                );
            Owner owner1 = new Owner();
            var robots = new List<Robot.Common.Robot>()
            {
            new Robot.Common.Robot(){ Energy = 100, Position = new Position(5 , 10), OwnerName = owner1.Name}
            };
            var command = myAlg.DoStep(robots, 0, map);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]

        public void TestindNearestFreeStation() // ����� ��������� ����� �������
        {
            var algo = new MyskivSerhiiAlgorithm();
            var map = new Map();
            var my = new Owner();
            var alien = new Owner();
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(5, 5), RecoveryRate = 2 });
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(10, 10), RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
        { new Robot.Common.Robot() { Energy = 2000, Position = new
        Position(10, 4),OwnerName = my.Name },
        new Robot.Common.Robot() { Energy = 200, Position = new Position(10, 10),OwnerName = alien.Name } };
            var command = algo.DoStep(robots, 0, map);
            Assert.IsNotNull(algo.FindNearestFreeStation(robots[0], map, robots));
        }

        [TestMethod]
        public void TestFindBestStation() // ����� �������� �������
        {
            var algo = new MyskivSerhiiAlgorithm();
            var map = new Map();
            map.Stations.Add(new EnergyStation() { Energy = 150, Position = new Position(5, 5), RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 250, Position = new Position(11, 5), RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 500, Position = new Position(4, 7), RecoveryRate = 10 });
            map.Stations.Add(new EnergyStation() { Energy = 50, Position = new Position(0, 0), RecoveryRate = 10 });
            var robots = new List<Robot.Common.Robot>
        {
            new Robot.Common.Robot() {Energy = 100, Position = new Position(7, 7)}
        };
            var expectedPosition = new Position(5, 5);
            var command = algo.DoStep(robots, 0, map);
            Assert.AreEqual(((MoveCommand)command).NewPosition, expectedPosition);
        }


        [TestMethod]
        public void TestRoundNumber() //���� �� ������� ������
        {
            var alg = new MyskivSerhiiAlgorithm();
            var numberOfRounds = 3;
            for (int i = 0; i < numberOfRounds; i++)
                Logger.LogRound(i);
            Assert.AreEqual(3, alg.RoundCount);
        }
    }
}
